//o para cambiar al ejecutar se escribe PUERTO=4500 node .\index.js
//para el nodemon
//node node_modules\nodemon\bin\nodemon.js index.js

const express = require('express')
const app = express()
const bodyParse = require('body-parser')

//Definir lista de recursos
var recursos=[{
    color:'azul',
    rgb:'#0000ff'
}];

//transforma a JSON
app.use(bodyParse.json());

app.get('/colores', function (req,res) {
    res.json(recursos);
})

//pedir la variable de entorno env
app.listen(process.env.PUERTO,()=>{
console.log('Escuchando en el puerto: '+ process.env.PUERTO);
})

//logica para agregar recurso
app.post('/colores',function (req, res) {
    //tomamos los datos del body 
    nuevo_color = req.body;
    //los agregamos a los recursos
    recursos.push(nuevo_color);
    //respuesta al usuario
    res.json({mensaje: 'Todo ok.'});

})

app.delete('/colores',function (req,res) {
    recursos = [];
    res.json({mensaje:'Todo ok.'})

})

//Tomar un valor dinámico
app.get('/colores/:color',function (req,res) {
    for (const elem of recursos) {
        if(elem.color == req.params.color){
            res.json(elem);
            return;
        }
        
    }
    res.status(404);
    res.json({mensaje: 'Color no encontrado'});
})

//actualizar un color
app.put('/colores/:color', function (req, res) {
    for (let index = 0; index < recursos.length; index++) {
        if(recursos[index].color==req.params.color){
            if(req.body && req.body.rgb){
                
                recursos[index].rgb =req.body.rgb;
                res.json({mensaje: "Elemento actualizado"})
                return;
            }
            else{
                res.status(400);
                res.json({mensaje: "Error"})
                return;
                
            }
        }      
    }
    res.status(404);
    res.json({mensaje: 'Color no encontrado'});

})

//eliminar un color
app.delete('/colores/:color', function (req, res) {
    for (let index = 0; index < recursos.length; index++) {
        if(recursos[index].color==req.params.color){
            recursos.splice(index,1);
            res.json({mensaje: 'Elemento borrado'})
            return;
        }
    }
    res.status(404);
    res.json({mensaje: 'Color no encontrado'});

} )

//implementar persistencia




